import apiClient from "@/utils/apiClient.js";

export const fetchUsers = () => {
    return apiClient.get('api/test-api/')
        .then(response => {
            return response.data;
        })
        .catch(error => {
            throw new Error(`API ${error}`);
        });
}

export const fetchAssets = () => {
    return apiClient.get('api/assets/')
        .then(response => {
            return response.data;
        })
        .catch(error => {
            throw new Error(`API ${error}`);
        });
}

export const fetchEmployees = () => {
    return apiClient.get('api/employees/')
        .then(response => {
            return response.data;
        })
        .catch(error => {
            throw new Error(`API ${error}`);
        });
}