import {createRouter, createWebHistory} from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from "@/views/LoginView.vue";
import AssetView from "@/views/AssetView.vue";
import EmployeeView from "@/views/EmployeeView.vue";
import RequestAssetView from "@/views/RequestAssetView.vue";
import ManageRequestsView from "@/views/ManageRequestsView.vue";
import ManageEmployeeView from "@/views/ManageEmployeeView.vue";
import ManageMaintenanceRequestsView from "@/views/ManageMaintenanceRequestsView.vue";

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [

        {
            path: '/login',
            name: 'LoginView',
            component: LoginView
        },
        {
            path: '/',
            name: 'Home',
            component: HomeView,
            meta: {requiresAuth: true}
        },
        {
            path: '/assets',
            name: 'Assets',
            component: AssetView, // Define this component to show assets
            meta: {requiresAuth: true}
        },
        {
            path: '/request-assets',
            name: 'RequestAssets',
            component: RequestAssetView, // Define this component to show assets
            meta: {requiresAuth: true}
        },
        {
            path: '/employees',
            name: 'Employees',
            component: EmployeeView, // Define this component to show assets
            meta: {requiresAuth: true, requiresSuperUser: true,}
        },
        {
            path: '/employee/:id',
            name: 'ManageEmployee',
            component: ManageEmployeeView,
            props: true,
            meta: {requiresAuth: true, requiresSuperUser: true,}
        },
        {
            path: '/manage-requests',
            name: 'ManageRequests',
            component: ManageRequestsView, // Define this component to show assets
            meta: {requiresAuth: true, requiresSuperUser: true,}
        },
        {
            path: '/manage-maintenance-requests',
            name: 'ManageMaintenanceRequestsView',
            component: ManageMaintenanceRequestsView, // Define this component to show assets
            meta: {requiresAuth: true, requiresSuperUser: true,}
        },
    ]
})

router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    const requiresSuperUser = to.matched.some(record => record.meta.requiresSuperUser);
    const isAuthenticated = localStorage.getItem('token');
    const userData = JSON.parse(sessionStorage.getItem('userData') || '{}');

    if (requiresAuth && !isAuthenticated) {
        next('/login');
    } else if (requiresSuperUser && !(userData.role === 'admin' || userData.role === 'it_staff')) {
        next('/'); // Redirect to home or another appropriate page
    } else {
        next();
    }
});


export default router
