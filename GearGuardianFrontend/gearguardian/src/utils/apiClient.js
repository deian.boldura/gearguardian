import axios from 'axios';
import Cookies from 'js-cookie'; // npm install js-cookie

const apiClient = axios.create({
    baseURL: import.meta.env.VITE_API_BASE_URL,
    withCredentials: true,
});

apiClient.interceptors.request.use(config => {
    const token = localStorage.getItem('token');
    const csrfToken = Cookies.get('csrftoken');  // Django sets this cookie automatically
    if (token) {
        config.headers.Authorization = `Token ${token}`;
    }
    if (csrfToken) {
        config.headers['X-CSRFToken'] = csrfToken;
    }
    return config;
});

export default apiClient;
export function setAuthToken() {
    const token = localStorage.getItem('token');
    if (token) {
        apiClient.defaults.headers.common['Authorization'] = 'Token ' + token;
    } else {
        delete apiClient.defaults.headers.common['Authorization'];
    }
}

export function deleteAuthToken() {
    let token = localStorage.getItem('token');
    let userData = JSON.parse(sessionStorage.getItem('userData'));
    if (token)
        localStorage.removeItem('token')
        delete apiClient.defaults.headers.common['Authorization'];  // Remove the token from default headers
        window.location.href = import.meta.env.VITE_APP_BASE_URL;
        window.location.reload();

    if (userData) {
        sessionStorage.removeItem('userData')
    }
}