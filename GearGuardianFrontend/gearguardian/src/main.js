import './assets/main.css'
import 'vuetify/styles'
import {createVuetify} from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import {ro} from 'vuetify/locale'

import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'; // Import the store you just created

import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import '@fortawesome/fontawesome-free/css/solid.min.css';
import '@mdi/font/css/materialdesignicons.css'

const gearGuardianTheme = {
    colors: {
        background: '#FFFFFF',
        surface: '#FFFFFF',
        primary: '#6200EE',
        'primary-darken-1': '#3700B3',
        secondary: '#03DAC6',
        'secondary-darken-1': '#018786',
        error: '#B00020',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FB8C00',
    },
}

const vuetify = createVuetify({
    theme: {
        defaultTheme: 'gearGuardianTheme',
        themes: {
            gearGuardianTheme,
        },
    },
    components,
    directives,
})


const app = createApp(App)

app.use(router);

app.use(store); // Use the Vuex store

app.use(vuetify);

app.mount('#app')
