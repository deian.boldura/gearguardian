import {createStore} from 'vuex';

export default createStore({
    state: {
        userData: null,
        drawerState: false, // State to control the visibility of the navigation drawer
        drawerItems: [  // Items for the drawer to dynamically create links
            {title: 'Home', icon: 'mdi-home', path: '/', name: 'Home'},
            {title: 'My Assets', icon: 'mdi-view-dashboard', path: '/assets', name: 'Assets'},
            {
                title: 'Employees',
                icon: 'mdi-account-group',
                path: '/employees',
                name: 'Employees',
                requiresSuperUser: true,
            },
            {title: 'Request assets', icon: 'mdi-plus-box-multiple', path: '/request-assets', name: 'RequestAssets'},
            {
                title: 'Manage asset requests',
                icon: 'mdi-format-list-bulleted',
                path: '/manage-requests',
                name: 'ManageRequests',
                requiresSuperUser: true,
            },
            {
                title: 'Manage maintenance requests',
                icon: 'mdi-wrench-check-outline',
                path: '/manage-maintenance-requests',
                name: 'ManageMaintenanceRequestsView',
                requiresSuperUser: true,
            },
            // Add additional items as needed
        ],
    },
    mutations: {
        setUserData(state, userData) {
            state.userData = userData;
        }
    },
    actions: {
        initializeUserData({commit}) {
            commit('setUserData', JSON.parse(sessionStorage.getItem('userData')));
        }
    },
    getters: {
        // Get the items for the drawer
        drawerItems: state => state.drawerItems,
        userData: state => state.userData,
    }
});
