# Pasi pentru rularea aplicatiilor (front-end si back-end)
Acest proiect a fost realizat utilizand Ubuntu 22.04 (Linux),
iar comenzile de mai jos sunt pentru acest mediu de dezvoltare.

# Rulare front-end
```sh
cd /GearGuardianFrontend/gearguardian
```

```sh
npm install
```

```sh
cd /GearGuardianFrontend/gearguardian
```

```sh
npm run dev
```

acum aplicatia de front-end poate fi accesata la http://localhost:5173


# Rulare back-end

```sh
cd /GearGuardian
```

```sh
pip install -r requirements.txt
```

```sh
source venv/bin/activate
```

```sh
python manage.py runserver
```



# gearguardian

## Recommended IDE Setup

PyCharm

