from django.db import models
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    class Role(models.TextChoices):
        ADMIN = 'admin', 'Admin'
        IT_STAFF = 'it_staff', 'IT Staff'
        EMPLOYEE = 'employee', 'Employee'

    role = models.CharField(max_length=50, choices=Role.choices, default=Role.EMPLOYEE)


class Asset(models.Model):
    name = models.CharField(max_length=255)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)