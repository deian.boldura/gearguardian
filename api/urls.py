from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import AssetViewSet, test_api, UserDetailView, EmployeeListView, UnassignedAssetViewSet, AssetRequestViewSet, MaintenanceRequestViewSet

router = DefaultRouter()
router.register(r'assets', AssetViewSet, basename='assets')
router.register(r'unassigned-assets', UnassignedAssetViewSet, basename='unassigned-assets')
router.register(r'asset-requests', AssetRequestViewSet, basename='asset-requests')
router.register(r'maintenance-requests', MaintenanceRequestViewSet, basename='maintenance-requests')

urlpatterns = [
    path('test-api/', test_api, name='test_api'),
    # Include Django REST Framework auth URLs for browsable API
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    # Include django-rest-auth URLs for REST API authentication handling
    path('rest-auth/', include('dj_rest_auth.urls')),
    path('user/', UserDetailView.as_view(), name='user-detail'),
    path('employees/', EmployeeListView.as_view(), name='employee-list'),
    path('', include(router.urls)),
]
