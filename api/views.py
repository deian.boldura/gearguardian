from rest_framework.response import Response
from rest_framework.decorators import api_view, action
from rest_framework import viewsets, status
from assets.models import Asset, AssetRequest, MaintenanceRequest
from assets.serializers import AssetSerializer, AssetRequestSerializer, MaintenanceRequestSerializer

from rest_framework import generics, permissions
from django.contrib.auth import get_user_model
from .serializers import UserSerializer
from django.utils.timezone import now


User = get_user_model()


class IsAdminOrITStaff(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.is_authenticated and (request.user.is_admin or request.user.role == 'it_staff')


class UserDetailView(generics.RetrieveAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = UserSerializer

    def get_object(self):
        # Return the current user, ensuring a user can only access their own data
        return self.request.user


@api_view(['GET'])
def test_api(request):
    if request.method == 'GET':
        users = get_user_model().objects.all()
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data)


class AssetViewSet(viewsets.ModelViewSet):
    serializer_class = AssetSerializer
    permission_classes = [permissions.IsAuthenticated]  # Ensure user is authenticated

    def get_queryset(self):
        # Check if the logged-in user is an Admin
        if self.request.user.role == User.Role.ADMIN:
            # If Admin, return all assets
            return Asset.objects.all()
        else:
            # Otherwise, return only assets assigned to the logged-in user
            return Asset.objects.filter(user=self.request.user)

    @action(detail=False, methods=['get'], permission_classes=[permissions.IsAdminUser])
    def by_employee(self, request):
        user_id = request.query_params.get('user_id')
        if user_id is not None:
            assets = Asset.objects.filter(user__id=user_id)
            serializer = self.get_serializer(assets, many=True)
            return Response(serializer.data)
        return Response({"error": "User ID parameter is required"}, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['post'], permission_classes=[permissions.IsAdminUser])
    def unassign(self, request, pk=None):
        asset = self.get_object()
        if asset.user is not None:
            asset.user = None
            asset.status = 'in_stock'  # Optionally reset the status
            asset.save()
            return Response({'status': 'unassigned'}, status=status.HTTP_200_OK)
        return Response({'error': 'Asset is already unassigned'}, status=status.HTTP_400_BAD_REQUEST)


class EmployeeListView(generics.ListAPIView):
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]  # Or any other permissions you deem necessary

    def get_queryset(self):
        # Filter the users to only those who have the role of 'Employee'
        return User.objects.filter(role=User.Role.EMPLOYEE)


class UnassignedAssetViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Asset.objects.filter(status='in_stock')
    serializer_class = AssetSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        """
        Optionally restricts the returned assets to a given user,
        by filtering against a `username` query parameter in the URL.
        """
        queryset = super().get_queryset()
        username = self.request.query_params.get('username')
        if username is not None:
            queryset = queryset.filter(user__username=username)
        return queryset


class AssetRequestViewSet(viewsets.ModelViewSet):
    queryset = AssetRequest.objects.all()
    serializer_class = AssetRequestSerializer
    permission_classes = [permissions.IsAuthenticated]

    def create(self, request, *args, **kwargs):
        request.data['user'] = request.user.id  # Automatically assign the requesting user
        return super().create(request, *args, **kwargs)

    @action(detail=True, methods=['post'], permission_classes=[permissions.IsAdminUser])
    def approve(self, request, pk=None):
        asset_request = self.get_object()
        asset = asset_request.asset

        if asset.status == 'in_stock':  # Check if the asset is available for assignment
            # Assign the asset to the user who made the request
            asset.user = asset_request.user
            asset.status = 'assigned'  # Update the status to indicate the asset is no longer in stock
            asset.last_assigned_user = asset_request.user
            asset.last_assigned_date = now()  # Record the time of assignment

            asset.save()  # Save the updates to the asset

            # Update the asset request to reflect approval
            asset_request.status = 'approved'
            asset_request.response_date = now()  # Record the response time
            asset_request.save()

            return Response({'status': 'approved'}, status=status.HTTP_200_OK)
        else:
            return Response({'error': 'Asset is not available'}, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['post'], permission_classes=[permissions.IsAdminUser])
    def decline(self, request, pk=None):
        asset_request = self.get_object()
        asset_request.status = 'denied'
        asset_request.response_date = now()  # Record the response time
        asset_request.save()

        return Response({'status': 'denied'}, status=status.HTTP_200_OK)


class MaintenanceRequestViewSet(viewsets.ModelViewSet):
    queryset = MaintenanceRequest.objects.all()
    serializer_class = MaintenanceRequestSerializer
    permission_classes = [permissions.IsAuthenticated]

    def create(self, request, *args, **kwargs):
        asset_id = request.data.get('asset_id')
        if not asset_id:
            return Response({'error': 'Asset ID is required'}, status=status.HTTP_400_BAD_REQUEST)

        try:
            asset = Asset.objects.get(id=asset_id)
        except Asset.DoesNotExist:
            return Response({'error': 'Asset not found'}, status=status.HTTP_404_NOT_FOUND)

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(asset=asset, user=request.user)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    @action(detail=True, methods=['post'], permission_classes=[permissions.IsAdminUser])
    def accept(self, request, pk=None):
        maintenance_request = self.get_object()
        if maintenance_request.status == 'open':
            resolution_date = request.data.get('resolution_date')
            if resolution_date:
                maintenance_request.accept_request(resolution_date=resolution_date)
            else:
                maintenance_request.accept_request()
            return Response({'status': 'in_progress'}, status=status.HTTP_200_OK)
        return Response({'error': 'Request is not in a state that can be accepted'}, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['post'], permission_classes=[permissions.IsAdminUser])
    def refuse(self, request, pk=None):
        maintenance_request = self.get_object()
        if maintenance_request.status == 'open':
            maintenance_request.refuse_request()
            return Response({'status': 'refused'}, status=status.HTTP_200_OK)
        return Response({'error': 'Request is not in a state that can be refused'}, status=status.HTTP_400_BAD_REQUEST)
