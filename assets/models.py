from django.db import models
from django.conf import settings  # Import settings to reference the AUTH_USER_MODEL


class Asset(models.Model):
    ASSET_TYPE_CHOICES = [
        ('Laptop', 'Laptop'),
        ('Desktop', 'Desktop Computer'),
        ('Mouse', 'Mouse'),
        ('Keyboard', 'Keyboard'),
        ('Headset', 'Headset'),
        ('Cable', 'Connection Cable')
    ]
    STATUS_CHOICES = [
        ('in_stock', 'In Stock'),
        ('assigned', 'Assigned')
    ]
    asset_type = models.CharField(max_length=15, choices=ASSET_TYPE_CHOICES)
    brand = models.CharField(max_length=50)
    model = models.CharField(max_length=50)
    serial_number = models.CharField(max_length=100, unique=True)
    purchase_date = models.DateField()
    warranty_expiry_date = models.DateField()
    status = models.CharField(max_length=50, choices=STATUS_CHOICES, default='in_stock')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True,
                             related_name='assets')
    last_assigned_date = models.DateTimeField(null=True, blank=True)
    last_assigned_user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True,
                                           related_name='last_assigned_assets')

    def __str__(self):
        return f"{self.asset_type} - {self.brand} {self.model} (SN: {self.serial_number})"


class AssetRequest(models.Model):
    STATUS_CHOICES = [
        ('requested', 'Requested'),
        ('approved', 'Approved'),
        ('denied', 'Denied'),
        ('cancelled', 'Cancelled')
    ]

    asset = models.ForeignKey('Asset', on_delete=models.CASCADE, related_name='requests')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='asset_requests')
    request_date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default='requested')
    response_date = models.DateTimeField(null=True, blank=True)  # Date when request was processed
    notes = models.TextField(blank=True, null=True)  # Optional notes by the admin

    def __str__(self):
        return f"{self.user.username} requests {self.asset.asset_type} - {self.asset.brand}"


class MaintenanceRequest(models.Model):
    MAINTENANCE_STATUS_CHOICES = [
        ('open', 'Open'),
        ('in_progress', 'In Progress'),
        ('completed', 'Completed'),
        ('closed', 'Closed'),
        ('refused', 'Refused')
    ]

    asset = models.ForeignKey('Asset', on_delete=models.CASCADE, related_name='maintenance_requests')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='maintenance_requests')
    description = models.TextField()
    request_date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=20, choices=MAINTENANCE_STATUS_CHOICES, default='open')
    resolution_date = models.DateTimeField(null=True, blank=True)

    def accept_request(self, resolution_date=None):
        self.status = 'in_progress'
        if resolution_date:
            self.resolution_date = resolution_date
        self.save()

    def refuse_request(self):
        self.status = 'refused'
        self.save()

    def __str__(self):
        return f"{self.asset.brand} {self.asset.model} maintenance requested by {self.user.username}"
