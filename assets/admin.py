from django.contrib import admin
from .models import Asset, AssetRequest, MaintenanceRequest
from accounts.models import CustomUser


class AssetAdmin(admin.ModelAdmin):
    list_display = ['asset_type', 'brand', 'model', 'serial_number', 'user', 'status', 'last_assigned_user',
                    'last_assigned_date']
    list_filter = ['asset_type', 'brand', 'status', 'user']
    search_fields = ['brand', 'model', 'serial_number', 'user__username']
    autocomplete_fields = ['user']  # This ensures the user field has autocomplete.

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser or request.user.role == CustomUser.Role.ADMIN:
            return qs
        return qs.filter(user=request.user)

    def get_readonly_fields(self, request, obj=None):
        if request.user.is_superuser or request.user.role in [CustomUser.Role.ADMIN, CustomUser.Role.IT_STAFF]:
            return []
        return ['last_assigned_user', 'last_assigned_date']  # Ensure these are always read-only

    def get_form(self, request, obj=None, **kwargs):
        form = super(AssetAdmin, self).get_form(request, obj, **kwargs)
        if 'user' in form.base_fields:
            form.base_fields['user'].required = False  # This makes the user field not required
        return form

    def has_change_permission(self, request, obj=None):
        if obj is not None and request.user.role == CustomUser.Role.EMPLOYEE:
            return False
        return super().has_change_permission(request, obj)

    def has_add_permission(self, request):
        return request.user.is_superuser or request.user.role in [CustomUser.Role.ADMIN, CustomUser.Role.IT_STAFF]

    def has_delete_permission(self, request, obj=None):
        return request.user.is_superuser or request.user.role == CustomUser.Role.ADMIN


admin.site.register(Asset, AssetAdmin)


class AssetRequestAdmin(admin.ModelAdmin):
    list_display = ['asset', 'user', 'request_date', 'status', 'response_date']
    list_filter = ['status', 'request_date', 'response_date']
    search_fields = ['asset__brand', 'asset__model', 'user__username', 'status']
    autocomplete_fields = ['asset', 'user']

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser or request.user.role == CustomUser.Role.ADMIN:
            return qs
        return qs.filter(user=request.user)

    def get_readonly_fields(self, request, obj=None):
        if request.user.is_superuser or request.user.role in [CustomUser.Role.ADMIN, CustomUser.Role.IT_STAFF]:
            return []
        return ['asset', 'user', 'request_date', 'status', 'response_date']  # Ensure these are always read-only


admin.site.register(AssetRequest, AssetRequestAdmin)


class MaintenanceRequestAdmin(admin.ModelAdmin):
    list_display = ['asset', 'user', 'description', 'request_date', 'status', 'resolution_date']
    list_filter = ['status', 'request_date', 'resolution_date']
    search_fields = ['asset__brand', 'asset__model', 'user__username', 'status', 'description']
    autocomplete_fields = ['asset', 'user']

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser or request.user.role == CustomUser.Role.ADMIN:
            return qs
        return qs.filter(user=request.user)

    def get_readonly_fields(self, request, obj=None):
        if request.user.is_superuser or request.user.role in [CustomUser.Role.ADMIN, CustomUser.Role.IT_STAFF]:
            return []
        return ['asset', 'user', 'request_date', 'status', 'resolution_date']  # Ensure these are always read-only


admin.site.register(MaintenanceRequest, MaintenanceRequestAdmin)
