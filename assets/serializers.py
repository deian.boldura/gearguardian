from rest_framework import serializers
from .models import Asset, AssetRequest, MaintenanceRequest
from django.contrib.auth import get_user_model

User = get_user_model()


class AssetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Asset
        fields = '__all__'


class UserSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username']  # Assuming you just want to display the username


class AssetSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Asset
        fields = ['id', 'brand', 'model']


class AssetRequestSerializer(serializers.ModelSerializer):
    user = UserSimpleSerializer(read_only=True)
    asset = AssetSerializer(read_only=True)
    asset_id = serializers.PrimaryKeyRelatedField(
        write_only=True,
        queryset=Asset.objects.all(),
        source='asset'
    )

    class Meta:
        model = AssetRequest
        fields = '__all__'
        read_only_fields = ['user', 'request_date', 'status', 'response_date', 'notes']

    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        validated_data['status'] = 'requested'
        return super().create(validated_data)


class MaintenanceRequestSerializer(serializers.ModelSerializer):
    user = UserSimpleSerializer(read_only=True)
    asset = AssetSerializer(read_only=True)

    class Meta:
        model = MaintenanceRequest
        fields = '__all__'
        read_only_fields = ['user', 'status']
