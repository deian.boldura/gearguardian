from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.timezone import now
from .models import Asset


@receiver(pre_save, sender=Asset)
def update_asset_assignment_info(sender, instance, **kwargs):
    if instance.pk:
        old_instance = Asset.objects.get(pk=instance.pk)
        # Check if the user field is changing
        if old_instance.user != instance.user:
            instance.last_assigned_user = instance.user
            instance.last_assigned_date = now()
            # Update the asset status based on whether it's being assigned or unassigned
            if instance.user:
                instance.status = 'assigned'
            else:
                instance.status = 'in_stock'